import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ListRestaurants()
    );
  }
}

class ListRestaurants extends StatefulWidget{
  @override
  createState() => _ListRestaurantsState();
}

class _ListRestaurantsState extends State<ListRestaurants> {
  var restaurants = new List<dynamic>();

  getRestaurants() {
    http.get('http://forkittest.gigalixirapp.com/api/restaurants').then((response){
      setState((){
        var result = json.decode(response.body);
        print(result['data']);
        restaurants = result['data'].map((rest) => Restaurant.fromJson(rest)).toList();
      });
    });
  }

  initState() {
    super.initState();
    getRestaurants();
  }

  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Search Restaurant'),
      ),
      body: ListView.builder(
        itemCount: restaurants.length,
        itemBuilder: (c, i){
          return RestaurantCard(restaurant: restaurants[i]);
        }
       ),
    );
  }
}

class Restaurant {
  final int id;
  final String city;
  final String street;

  Restaurant({this.id, this.city, this.street});

  factory Restaurant.fromJson(Map<String, dynamic> json){
    return Restaurant(
      id: json['id'],
      city: json['address']['city'],
      street: json['address']['street']
    );
  }
}


class RestaurantCard extends StatelessWidget {
  final Restaurant restaurant;

  RestaurantCard({Key key, @required this.restaurant}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    print(restaurant.id.toString());
    return Container(
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              child: Image.asset('images/restaurant'+this.restaurant.id.toString()+'.jpg',fit: BoxFit.cover,),
              height: 180,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 10, 10),
                    child: Text(this.restaurant.city, textAlign: TextAlign.left, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                    child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                                child:Text('Aberto', textAlign: TextAlign.right, style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold, fontSize: 16))
                        )
                  )
                )
              ],
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 0, 10),
                child: Container(
                      child: Text(this.restaurant.street, style: TextStyle(fontSize: 16)),
              ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
